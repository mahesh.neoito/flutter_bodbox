import 'package:flutter/services.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:wifi_hunter/wifi_hunter.dart';

class WifiConnect extends StatefulWidget {
  @override
  _WifiConnectState createState() => _WifiConnectState();
}

class _WifiConnectState extends State<WifiConnect> {
  WiFiInfoWrapper _wifiObject;

  @override
  void initState() {
    scanWiFi();
    super.initState();
  }

  Future<WiFiInfoWrapper> scanWiFi() async {
    WiFiInfoWrapper wifiObject;

    try {
      wifiObject = await WiFiHunter.huntRequest;
    } on PlatformException {}

    return wifiObject;
  }

  Future<void> scanHandler() async {
    _wifiObject = await scanWiFi();
    print("WiFi Results (SSIDs) : ");
    for (var i = 0; i < _wifiObject.ssids.length; i++) {
      print("- " + _wifiObject.ssids[i]);
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          appBar: AppBar(
            title: const Text('Connect To Wifi'),
          ),
          body: _wifiList()
          // body: listAvailableWifi.length==null? Container(child: Text('Searching For Wifi...'),): _bluetoothList()
          ),
    );
  }

  Widget _wifiList() {
    return FutureBuilder(
        future: scanHandler(),
        builder: (context, projectSnap) {
          if (projectSnap.connectionState == ConnectionState.none &&
              projectSnap.hasData == null) {
            return Container();
          } else {
            return Center(
                child: ListView.builder(
                    itemCount: _wifiObject.ssids.length,
                    scrollDirection: Axis.vertical,
                    itemBuilder: (BuildContext context, int index) {
                      return Column(
                        children: <Widget>[
                          ListTile(
                            title: Text(_wifiObject.ssids[index].toString()),
                            trailing: IconButton(
                              icon: Icon(Icons.more_vert),
                              onPressed: () {
                                showSimpleCustomDialog(context);
                              },
                            ),
                          ),
                        ],
                      );
                    }));
          }
        });
  }

//Modal Popup
  void showSimpleCustomDialog(BuildContext context) {
    Dialog simpleDialog = Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12.0),
      ),
      child: Container(
        height: 300.0,
        width: 300.0,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
                padding: EdgeInsets.all(15.0),
                child: Column(
                  children: <Widget>[
                    ListTile(
                      leading: const Icon(Icons.vpn_key),
                      title: TextFormField(
                        obscureText: true,
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please Enter Password';
                          }
                          return null;
                        },
                        keyboardType: TextInputType.text,
                        // controller: nameControler,
                        decoration: InputDecoration(
                          hintText: "Password",
                        ),
                      ),
                    ),
                    ListTile(
                      leading: const Icon(Icons.format_list_numbered),
                      title: TextFormField(
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please Enter BodBox ID';
                            // phoneControler.text = null;
                          }
                          // phoneControler.text = null;
                          return null;
                        },
                        keyboardType: TextInputType.text,
                        // controller: phoneControler,
                        decoration: InputDecoration(
                          hintText: "BodBox ID",
                        ),
                      ),
                    ),
                  ],
                )),
            Padding(
              padding: const EdgeInsets.only(left: 10, right: 10, top: 50),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  RaisedButton(
                    color: Colors.blue,
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      'Connect',
                      style: TextStyle(fontSize: 18.0, color: Colors.white),
                    ),
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  RaisedButton(
                    color: Colors.red,
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      'Cancel!',
                      style: TextStyle(fontSize: 18.0, color: Colors.white),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
    showDialog(
        context: context, builder: (BuildContext context) => simpleDialog);
  }
}

class BluetoothModel {
  String ssid;
  BluetoothModel({this.ssid});
}
