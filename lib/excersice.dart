import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutterbodbox/gif_ani.dart';
import 'package:quiver/async.dart';

class ExercisePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with TickerProviderStateMixin {
  String get timerString {
    Duration duration = controller.duration * controller.value;
    return '${duration.inMinutes}:${(duration.inSeconds % 60).toString().padLeft(2, '0')}';
  }

  GifController _animationCtrl;
  AnimationController controller;

  @override
  void initState() {
    getWorkoutDetails().then((results) {
      setState(() {
        var exercise = results;
        print(exercise.toString());
      });
    });
    setDuration(1, 60);
    super.initState();
  }

  @override
  void dispose() {
    _animationCtrl.dispose();
    super.dispose();
  }

  void setDuration(int dur, int fc) {
    _animationCtrl = GifController(
        vsync: this, duration: Duration(seconds: dur), frameCount: fc);
  }

  //Countdown Timer

  // int _start = 10;
  int _current = 10;

  void startTimer(int _start) {
    CountdownTimer countDownTimer = new CountdownTimer(
      new Duration(seconds: _start),
      new Duration(seconds: 1),
    );

    var sub = countDownTimer.listen(null);
    sub.onData((duration) {
      setState(() {
        _current = _start - duration.elapsed.inSeconds;
      });
    });
    sub.onDone(() {
      print("Done");
      // next exercise
      sub.cancel();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            _buildGif(),
            Row(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(5),
                ),
                RaisedButton.icon(
                  icon: Icon(Icons.satellite),
                  label: Text("PLAY"),
                  onPressed: () {
                    setState(() {
                      startTimer(10);
                      setDuration(10, 60);
                    });
                    _animationCtrl.runAni();
                  },
                ),
                Padding(
                  padding: EdgeInsets.all(5),
                ),
                RaisedButton.icon(
                  icon: Icon(Icons.satellite),
                  label: Text("Replay"),
                  onPressed: () {
                    setState(() {});
                  },
                ),
                Padding(
                  padding: EdgeInsets.all(5),
                ),
                _current == null ? Container() : Text("$_current")
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget _buildGif() {
    Widget ret = GifAnimation(
      image: NetworkImage(
          'https://firebasestorage.googleapis.com/v0/b/bodbox-25d69.appspot.com/o/gifs%2Flying%20leg%20raises.gif?alt=media&token=14a662ce-3169-4d79-94bc-1b4105cb822a'),
      controller: _animationCtrl,
    );
    return ret;
  }

  //Methods
  //Check Bluetooth Connectivity
  //Check If Logged In & Get BodboxID and User ID
  //Fetch Excercise Regime
  //Move To Next Exercise
  //Get WorkOut Details
  Future<List> getWorkoutDetails() async {
    List<DocumentSnapshot> templist;
    List list = new List();
    CollectionReference collectionRef = Firestore.instance
        .collection('excersices')
        .document('abdominals')
        .collection('bodyweight-g2');
    QuerySnapshot collectionSnapshot = await collectionRef.getDocuments();
    templist = collectionSnapshot.documents; // <--- ERROR
    list = templist.map((DocumentSnapshot docSnapshot) {
      return docSnapshot.data;
    }).toList();
    return list;
  }
}
